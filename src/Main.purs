module Main where

import Prelude
import Screeps.Types

import Data.Argonaut.Decode (printJsonDecodeError)
import Data.Either (Either(..))
import Data.Int (decimal, toStringAs)
import Data.Maybe (Maybe(..))
import Effect (Effect)
import Effect.Console (log)
import Screeps.Game as Game
import Screeps.Memory as Memory
import Shimmy.Creep (manageCreeps)
import Shimmy.Spawning (initSpawn)

main :: Effect Unit
main = do
  game <- Game.getGameGlobal
  memory <- Memory.getMemoryGlobal
  status <- isInitialized memory
  case status of
    Nothing -> do
      initLoop game memory LoopGo
    Just s -> do
      runLoop game memory s

-- | is initialized
isInitialized :: Memory.MemoryGlobal -> Effect (Maybe LoopStatus)
isInitialized memory = do
  status <- Memory.get memory "loopStatus"
  case status of
    Left err -> do
      log $ printJsonDecodeError err
      pure Nothing
    Right s -> case s of
      Nothing -> pure $ Just LoopGo
      Just s1 -> pure s1

-- | init loop
initLoop :: GameGlobal -> Memory.MemoryGlobal -> LoopStatus -> Effect Unit
initLoop game memory status = do
  log "init loop"
  Memory.set memory "loopStatus" $ Just status
  Memory.set memory "utility" 0
  let creeps = Game.creeps game
  initSpawn creeps game memory
  manageCreeps creeps game memory
  --


-- | run loop
runLoop :: GameGlobal -> Memory.MemoryGlobal -> LoopStatus -> Effect Unit
runLoop game memory status = do
  let creeps = Game.creeps game
      time = Game.time game
  log $ toStringAs decimal time
  manageCreeps creeps game memory