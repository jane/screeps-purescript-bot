"use_strict";

export const ok = OK;
export const err_not_owner = ERR_NOT_OWNER;
export const err_no_path = ERR_NO_PATH;
export const err_name_exists = ERR_NAME_EXISTS;
export const err_busy = ERR_BUSY;
export const err_not_found = ERR_NOT_FOUND;
export const err_not_enough_energy = ERR_NOT_ENOUGH_ENERGY;
export const err_not_enough_resources = ERR_NOT_ENOUGH_RESOURCES;
export const err_invalid_target = ERR_INVALID_TARGET;
export const err_full = ERR_FULL;
export const err_not_in_range = ERR_NOT_IN_RANGE;
export const err_invalid_args = ERR_INVALID_ARGS;
export const err_tired = ERR_TIRED;
export const err_no_bodypart = ERR_NO_BODYPART;
export const err_not_enough_extensions = ERR_NOT_ENOUGH_EXTENSIONS;
export const err_rcl_not_enough = ERR_RCL_NOT_ENOUGH;
export const err_gcl_not_enough = ERR_GCL_NOT_ENOUGH;

export const find_exit_top = FIND_EXIT_TOP;
export const find_exit_right = FIND_EXIT_RIGHT;
export const find_exit_bottom = FIND_EXIT_BOTTOM;
export const find_exit_left = FIND_EXIT_LEFT;
export const find_exit = FIND_EXIT;
export const find_creeps = FIND_CREEPS;
export const find_my_creeps = FIND_MY_CREEPS;
export const find_hostile_creeps = FIND_HOSTILE_CREEPS;
export const find_sources_active = FIND_SOURCES_ACTIVE;
export const find_sources = FIND_SOURCES;
export const find_dropped_energy = FIND_DROPPED_ENERGY;
export const find_dropped_resources = FIND_DROPPED_RESOURCES;
export const find_structures = FIND_STRUCTURES;
export const find_structures0 = FIND_STRUCTURES;
export const find_my_structures = FIND_MY_STRUCTURES;
export const find_hostile_structures = FIND_HOSTILE_STRUCTURES;
export const find_flags = FIND_FLAGS;
export const find_construction_sites = FIND_CONSTRUCTION_SITES;
export const find_my_spawns = FIND_MY_SPAWNS;
export const find_hostile_spawns = FIND_HOSTILE_SPAWNS;
export const find_my_construction_sites = FIND_MY_CONSTRUCTION_SITES;
export const find_hostile_construction_sites = FIND_HOSTILE_CONSTRUCTION_SITES;
export const find_minerals = FIND_MINERALS;
export const find_nukes = FIND_NUKES;

export const structure_spawn = STRUCTURE_SPAWN;
export const structure_extension = STRUCTURE_EXTENSION;
export const structure_road = STRUCTURE_ROAD;
export const structure_wall = STRUCTURE_WALL;
export const structure_rampart = STRUCTURE_RAMPART;
export const structure_keeper_lair = STRUCTURE_KEEPER_LAIR;
export const structure_portal = STRUCTURE_PORTAL;
export const structure_controller = STRUCTURE_CONTROLLER;
export const structure_link = STRUCTURE_LINK;
export const structure_storage = STRUCTURE_STORAGE;
export const structure_tower = STRUCTURE_TOWER;
export const structure_observer = STRUCTURE_OBSERVER;
export const structure_power_bank = STRUCTURE_POWER_BANK;
export const structure_power_spawn = STRUCTURE_POWER_SPAWN;
export const structure_extractor = STRUCTURE_EXTRACTOR;
export const structure_lab = STRUCTURE_LAB;
export const structure_terminal = STRUCTURE_TERMINAL;
export const structure_container = STRUCTURE_CONTAINER;
export const structure_nuker = STRUCTURE_NUKER;

export const pMove = MOVE;
export const pWork = WORK;
export const pCarry = CARRY;

export const resource_energy = RESOURCE_ENERGY;
export const resource_power = RESOURCE_POWER;

export const resource_hydrogen = RESOURCE_HYDROGEN;
export const resource_oxygen = RESOURCE_OXYGEN;
export const resource_utrium = RESOURCE_UTRIUM;
export const resource_lemergium = RESOURCE_LEMERGIUM;
export const resource_keanium = RESOURCE_KEANIUM;
export const resource_zynthium = RESOURCE_ZYNTHIUM;
export const resource_catalyst = RESOURCE_CATALYST;
export const resource_ghodium = RESOURCE_GHODIUM;

export const resource_hydroxide = RESOURCE_HYDROXIDE;
export const resource_zynthium_keanite = RESOURCE_ZYNTHIUM_KEANITE;
export const resource_utrium_lemergite = RESOURCE_UTRIUM_LEMERGITE;

export const resource_utrium_hydride = RESOURCE_UTRIUM_HYDRIDE;
export const resource_utrium_oxide = RESOURCE_UTRIUM_OXIDE;
export const resource_keanium_hydride = RESOURCE_KEANIUM_HYDRIDE;
export const resource_keanium_oxide = RESOURCE_KEANIUM_OXIDE;
export const resource_lemergium_hydride = RESOURCE_LEMERGIUM_HYDRIDE;
export const resource_lemergium_oxide = RESOURCE_LEMERGIUM_OXIDE;
export const resource_zynthium_hydride = RESOURCE_ZYNTHIUM_HYDRIDE;
export const resource_zynthium_oxide = RESOURCE_ZYNTHIUM_OXIDE;
export const resource_ghodium_hydride = RESOURCE_GHODIUM_HYDRIDE;
export const resource_ghodium_oxide = RESOURCE_GHODIUM_OXIDE;

export const resource_utrium_acid = RESOURCE_UTRIUM_ACID;
export const resource_utrium_alkalide = RESOURCE_UTRIUM_ALKALIDE;
export const resource_keanium_acid = RESOURCE_KEANIUM_ACID;
export const resource_keanium_alkalide = RESOURCE_KEANIUM_ALKALIDE;
export const resource_lemergium_acid = RESOURCE_LEMERGIUM_ACID;
export const resource_lemergium_alkalide = RESOURCE_LEMERGIUM_ALKALIDE;
export const resource_zynthium_acid = RESOURCE_ZYNTHIUM_ACID;
export const resource_zynthium_alkalide = RESOURCE_ZYNTHIUM_ALKALIDE;
export const resource_ghodium_acid = RESOURCE_GHODIUM_ACID;
export const resource_ghodium_alkalide = RESOURCE_GHODIUM_ALKALIDE;

export const resource_catalyzed_utrium_acid = RESOURCE_CATALYZED_UTRIUM_ACID;
export const resource_catalyzed_utrium_alkalide = RESOURCE_CATALYZED_UTRIUM_ALKALIDE;
export const resource_catalyzed_keanium_acid = RESOURCE_CATALYZED_KEANIUM_ACID;
export const resource_catalyzed_keanium_alkalide = RESOURCE_CATALYZED_KEANIUM_ALKALIDE;
export const resource_catalyzed_lemergium_acid = RESOURCE_CATALYZED_LEMERGIUM_ACID;
export const resource_catalyzed_lemergium_alkalide = RESOURCE_CATALYZED_LEMERGIUM_ALKALIDE;
export const resource_catalyzed_zynthium_acid = RESOURCE_CATALYZED_ZYNTHIUM_ACID;
export const resource_catalyzed_zynthium_alkalide = RESOURCE_CATALYZED_ZYNTHIUM_ALKALIDE;
export const resource_catalyzed_ghodium_acid = RESOURCE_CATALYZED_GHODIUM_ACID;
export const resource_catalyzed_ghodium_alkalide = RESOURCE_CATALYZED_GHODIUM_ALKALIDE;