"use strict";

// module Screeps.FFI

export function unsafeField(key){
    return function(obj){
        return obj[key];
    }
}
export function unsafeGetFieldEff(key){
    return function(obj){
        return function(){
            return obj[key];
        }
    }
}
export function unsafeSetFieldEff(key){
    return function(obj){
        return function(val){
            return function(){
                obj[key] = val;
            }
        }
    }
}
export function unsafeDeleteFieldEff(key){
  return function(obj){
      return function(){
        delete obj[key];
      }
  }
}
export function runThisEffFn0(key){
    return function(self){
        return function(){
            return self[key]();
        }
    }
}
export function runThisEffFn1(key){
    return function(self){
        return function(a){
            return function(){
                return self[key](a);
            }
        }
    }
}
export function runThisEffFn2(key){
  return function(self){
    return function(a){
      return function(b){
        return function(){
          return self[key](a, b);
        }
      }
    }
  }
}
export function runThisEffFn3(key){
  return function(self){
    return function(a){
      return function(b){
        return function(c){
          return function(){
            return self[key](a, b, c);
          }
        }
      }
    }
  }
}
export function runThisEffFn4(key){
  return function(self){
    return function(a){
      return function(b){
        return function(c){
          return function(d){
            return function(){
              return self[key](a, b, c, d);
            }
          }
        }
      }
    }
  }
}
export function runThisFn0(key){
  return function(self){
    return self[key]();
  }
}
export function runThisFn1(key){
  return function(self){
    return function(a){
      return self[key](a);
    }
  }
}
export function runThisFn2(key){
  return function(self){
    return function(a){
      return function(b){
        return self[key](a,b);
      }
    }
  }
}
export function runThisFn3(key){
  return function(self){
    return function(a){
      return function(b){
        return function(c){
          return self[key](a,b,c);
        }
      }
    }
  }
}
export const _null = null;
export const _undefined = undefined
export function notNullOrUndefined(x){
    return x;
}
export function isNull(x){
    return x === null;
}
export function isUndefined(x){
    return x === undefined;
}
export function toMaybeImpl (val, nothing, just){
    if(val === null || val === undefined){
        return nothing;
    } else {
        return just(val);
    }
}
export function unsafeGetAllCreepEff(creep){
    return function(){
        return Memory.creeps[creep];
    }
}
export function unsafeGetCreepEff(creep,key){
    return function(){
        return Memory.creeps[creep][key];
    }
}
export function unsafeSetCreepEff(creep,key){
    return function(val){
        return Memory.creeps[creep][key] = val;
    }
}
// export function rawSpawnCreep(){
//   return function (self){
//     return function(spawn){
//       return function(parts){
//         return function(name){
//           return function(r){
//               console.log("spawning creep...");
//               return self.spawns[spawn].spawnCreep(parts,name,{memory: {role: r, utility: 1}});
//           }
//         }
//       }
//     }
//   }
// }