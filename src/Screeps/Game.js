"use strict";

export function getGameGlobal(){ return Game; }
export function createCreepImpl(structure){
    return function(parts){
      return function(role){
        return function(left){
          return function(right){
            return function(){
              var result = structure.createCreep(parts,{utility: 1, role: role});
              if (typeof result === "string"){
                  return right(result);
              } else {
                  return left(result);
              }
            }
          }
        }
      }
    }
}
export function rawGetUtility(key){
    return function(obj){
        return obj.creeps[key].utility;
    }
}

export function rawGetRole(key){
    return function(obj){
        return obj.creeps[key].role;
    }
}