"use strict";

export function getMemoryGlobal(){ return Memory; }
export function getRawMemoryGlobal(){ return RawMemory; }
export function getCreepsUtl(){
  var res = []
  for (var c in Memory.creeps) {
    res = res.concat(Memory.creeps[c]["utility"])
  }
  return res
}
export function getCreepsNames(){
  var res = []
  for (var c in Memory.creeps) {
    res += c
  }
  return res
}
export function setCreepsUtl(key,n){
        return Memory.creeps[key] = n
}