"use strict";

export function spawnStoreCapacity(obj){
    var ret = obj.store;
    if (typeof ret !== 'undefined')
        return ret.getFreeCapacity(RESOURCE_ENERGY);
}
export function spawnStoreEnergy(obj){
    var ret = obj.store;
    if (typeof ret !== 'undefined')
        return ret[RESOURCE_ENERGY];
}