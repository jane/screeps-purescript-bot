module Shimmy.Creep where

import Prelude
import Screeps.Types

import Data.Maybe (Maybe(..))
import Effect (Effect)
import Foreign.Object as F
import Screeps.Game as Game
import Screeps.Memory as Memory
import Shimmy.Spawning (doSpawnCheck)

manageCreeps :: F.Object Creep -> GameGlobal -> Memory.MemoryGlobal -> Effect Unit
manageCreeps hash game mem = do
  let spawns = Game.spawns game
  -- TODO: iterate over all spawns?
      spawn1 = F.lookup "Spawn1" spawns
  case spawn1 of
    Nothing -> pure unit
    Just s1 -> doSpawnCheck s1 (F.size hash) mem